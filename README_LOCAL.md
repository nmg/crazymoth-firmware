This is the local README for the UMD IBIS Lab Crazyflie "Crazymoth" project. 

In order to use the firmware effectively, a good understanding of git is
crucial. Run "man gittutorial" to learn more about how git can be used. 

Currently, there are 2 branches. The first is master, and tracks the
official bitcraze crazyflie 2 firmware. The second branch is autonomous, which
has firmware features that allow autonomous control. 



## Resources

Bitcraze Forums


